# tiny-linux

A collection of scripts and configs for creating a tiny linux system.

The goal is to get to at least 2.88 MBs with a working system.

Hardware-wise, this project is mainly focused on Thinkpad X230, but by swapping drivers in the config, it will run on pretty much any PC.

### Building

Initramfs/root and the kernel (complete os):
```
make prepare
make
```
Initramfs/root :  ```make initramfs```

Kernel: ```make kernel```

Busybox: ```make busybox```

"Refresh" source folders: ```make refresh```

Apply configs: ```make config```

Save current configs: ```make save ```

Test in QEMU: ```make test```