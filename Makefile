BUSYBOX_VER = 1.29.3
JOBS = 4

all:
	make config
	make busybox
	rm -rf out
	mkdir out
	make kernel
	make initramfs

refresh:
	rm -rf out initramfs src/busybox src/linux
	#Unpacking Linux
	tar -xvf dl/linux.tar.xz -C src
	mv src/linux-* src/linux
	#Unpacking BusyBox
	tar -xvf dl/busybox.tar.bz2 -C src
	mv src/busybox-* src/busybox

test:
	qemu-system-x86_64 -kernel out/kernel -initrd out/initramfs.cpio.gz

save:
	cp src/busybox/.config src/busybox_config 	
	cp src/linux/.config src/linux_config 

config:
	rm -rf src/busybox/.config src/linux/.config  
	cp src/busybox_config src/busybox/
	mv src/busybox/busybox_config src/busybox/.config
	cp src/linux_config src/linux/
	mv src/linux/linux_config src/linux/.config
kernel:
	rm -rf out/kernel
	#Building Linux
	make -j $(JOBS) -C src/linux
	cp src/linux/arch/x86/boot/bzImage out/kernel

busybox:
	#Building BusyBox	
	make -j $(JOBS) -C src/busybox

initramfs:
	#Building initramfs
	rm -rf initramfs out/initramfs.cpio.gz
	mkdir -p initramfs/bin initramfs/proc initramfs/sys initramfs/etc initramfs/sbin initramfs/usr/bin initramfs/usr/sbin
	cp src/init initramfs/sbin/init
	cp src/busybox/busybox initramfs/bin/busybox
	(cd initramfs/bin;ln -s busybox sh)
	rm -rf out/initramfs.cpio.gz
	(cd initramfs; find . -print0 | cpio --null -ov --format=newc | gzip -9 > ../out/initramfs.cpio.gz)	

prepare:
	rm -rf dl
	mkdir dl
	#Downloading Linux
	wget --show-progress -O dl/linux.tar.xz https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.18.16.tar.xz
	#Downloading BusyBox
	wget --show-progress -O dl/busybox.tar.bz2 https://busybox.net/downloads/busybox-$(BUSYBOX_VER).tar.bz2
	make refresh
